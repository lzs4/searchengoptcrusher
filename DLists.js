'use strict';

/* ALL */
const Strict_d = {
    'name': 'Strict',
    'description': 'アフィサイトに共通するワードを排除する',
    'level': 4,
    'category': 'ALL',
    'list': [
	/soku/, // 〇〇速報系
	/matome/, // まとめサイトに頻出
	/antenam/, // アンテナサイト
	/blog\.livedoor\.com/, // livedoorブログ
    ]
};

const Affiliate_d = {
    'name': 'Affiliate',
    'description': 'アフィカス',
    'level': 2,
    'category': 'ALL',
    'list': [
	/labo\.tv/, // まとめサイトのまとめ
	/jin115\.com/, // 俺的ゲーム速報
	/donsok\.com/, // ドンと来い速報
	/himasoku\.com/, // 暇人暇速
	/kanasoku\.info/, // カナ速
	/kanasoku\.coreserver\.jp/, // カナ速あんてな
	/hamusoku\.com/, // ハムスター速報(ハム速)
	/alfalfalfa\.com/, // アルファルファモザイク
	/matome-plus\.com/, // まとめサイト速報＋
	/yaraon-blog\.com/, // やらおん
	/blog\.esuteru\.com/, // はちま
	/beelzeboulxxx\.com/, // 魔王ブログ
	/wonderfulnews\.xyz/, // ワンダフルNewS!
	/2chmatome\.jpn\.org/, // まとめらいおん
	/magsoku\.blomaga\.jp/, // MAG速
	/brow2ing\.doorblog\.jp/, // ブラブラブラウジング
	/news4vip\.livedoor\.biz/, // ニュー速クオリティ
	/aatyu\.livedoor\.blog/, // ゲーハー
	/blog\.livedoor\.jp\/book0803/, // ゲーハー
	/blog\.livedoor\.jp\/dqnplus/, // 痛いニュース
	/blog\.livedoor\.jp\/insidears/, // ニュー速VIPブログ
    ]
};

const Matome_d = {
    'name': 'Matome',
    'description': '企業型まとめサイト',
    'level': 2,
    'category': 'ALL',
    'list': [
	/matome\.naver\.jp/, // naverまとめ
	/togetter\.com/, // togetter
    ]
};

const Media_d = {
    'name': 'Media',
    'description': '企業型のメディア',
    'level': 2,
    'category': 'ALL',
    'list': [
	/mery\.jp/, // MERY
	/nlab\.itmedia\.co\.jp/, // ねとらぼ
	/buzzfeed\.com/, // buzzfeed
    ]
};

const QandA_d = {
    'name': 'Q & A',
    'description': '質問サイト',
    'level': 2,
    'category': 'ALL',
    'list': [
	/chiebukuro\.yahoo\.co\.jp/, // 知恵袋
	/oshiete\.goo\.ne\.jp/, // 教えて!goo
    ]
};

const Horror_d = {
    'name': 'Horror',
    'description': '洒落怖関係のまとめサイト',
    'level': 2,
    'category': 'ALL',
    'list': [
	/nazolog\.com/, // 怖い話まとめブログ
    ]
};

/* PG */
const PGschool_d = {
    'name': 'PGschool',
    'description': 'プログラミングスクール系',
    'level': 3,
    'category': 'PG',
    'list': [
        /sejuku/, // 侍エンジニア塾
	/codecamp\.jp/, // CodeCamp
	/web-camp\.io/, // WEBCAMP
	/schoo\.jp/, // Schoo
        /tech-camp\.in/, // TECH::CAMP
	/tech-boost\.jp/, // TechBoost
	/techacademy\.jp/, // TechAcademy
	/proengineer\.internous\.co\.jp/, // プログラマカレッジ
	/diveintocode\.jp/, // DIVE INTO CODE
	/internetacademy\.jp/, // インターネット・アカデミー
    ]
};

const MTrans_d = {
    'name': 'MTrans',
    'description': '機会翻訳',
    'level': 3,
    'category': 'PG',
    'list': [
	/codeday\.org/, // コードログ
	/codeday\.me/, // コードログ
    ]
};

/* HIST */
let History_d = {
    'name': 'History',
    'description': 'ゲームやアニメなど除外',
    'level': 3,
    'category': 'HIST',
    'list': [
	/fgo/i, // fgo関連
	/fate/i, // fate関連
	/typemoon\.cre\.jp/, // TYPE-MOON Wiki
	/pprct\.net/, // ピプリクト
	/famitsu\.com/, // ファミ通
    ]
};

/* GAME */
const Game_d = {
    'name': 'Game',
    'description': '企業型ゲーム攻略サイトなど',
    'level': 3,
    'category': 'GAME, HIST',
    'list': [
	/gamy\.jp/, // GAMY
	/game8\.jp/, // Game8
	/altema\.jp/, // アルテマ
	/appmedia\.jp/, // AppMedia
	/gamewith\.jp/, // GameWith
	/appbank\.net/, // AppBank
	/appbank\.co\.jp/, // AppBank
	/kamigame\.jp/, // 神ゲー攻略
	/wazap\.com/, // ワザップ
    ]
};

const Pokemon_d = {
    'name': 'Pokemon',
    'description': 'ポケモンに関するまとめサイト',
    'level': 3,
    'category': 'GAME',
    'list': [
	/pokemon-matome\.net/, // ぽけりん
	/pokemonbbs\.com/, // ポケモンBBS
    ]
};

const SNGame_d = {
    'name': 'Social network game',
    'description': 'ソシャゲのアフィサイト',
    'level': 3,
    'category': 'GAME',
    'list': [
	/* パズドラ */
	/tpnav\.com/, // たまドラぷらすナビ
	/padmo\.net/, // パズドラの森
	/umauma\.net/, // ウマウマアンテナ
	/piedora\.com/, // ピエドラあんてな
	/pazudorax\.com/, // パズドラ考察ブログ
	/pazdra\.antenam\.info/, // パズドラまとめアンテナ
	/pazudora\.antenam\.info/, // パズドラまとめのまとめ
	/pazudora7\.antenam\.biz/, // 究極まとめアンテナ
	/pazudoraqa\.antenam\.biz/, // パズドラ総合アンテナ
	/pazudora24\.antenam\.info/, // パズドラ情報アンテナ
	/pazzledra\.antenam\.info/, // パズドラーあんてな
	/blog\.livedoor\.com\/category\/441/, // パズドラ - ライブドアブログ
	/* モンスト */
	/mnstmatome\.readers\.jp/, // モンストまとめアンテナ
    ]
};

/* ENT */
const Gossip_d = {
    'name': 'Gossip',
    'description': '芸能関係のアフィサイト',
    'level': 3,
    'category': 'ENT',
    'list': [
	/cobwebs\.jp/, // 芸能ニュースまとめ
    ]
};

/* Other */
const Skeleton_d = {
    'name': '',
    'description': '',
    'level': 3,
    'category': 'ALL',
    'list': [
    ]
};

const NGDomain = [
    Strict_d,
    Affiliate_d,
    Matome_d,
    Media_d,
    QandA_d,
    Horror_d,
    PGschool_d,
    History_d,
    Game_d,
    Pokemon_d,
    SNGame_d,
    Gossip_d
];

console.log(NGDomain);
