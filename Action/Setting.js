'use strict';

let Flags;
const Str = [
    'フィルタなし',
    '危険なサイトを除去',
    'まとめサイトを排除',
    'カテゴリごとのフィルタ',
    '強力なフィルタ'
];

const LevelMAX = 4, LevelMIN = 0;

class LevelPtr {
    constructor() {
	this._level = 0;
    }
    get State() {
	return this._level;
    }
    set LoadState(level) {
	this._level = level;
    }
    Minus () {
	if (this._level > LevelMIN) {
	    this._level--;
	}}
    Plus () {
	if (this._level < LevelMAX) {
	    this._level++;
	}
    }
};

let InitChild = parent => {
    if (parent.removeChild(parent.firstChild)) {
	while (parent.firstChild) {
	    parent.removeChild(parent.firstChild);
	}
    }
};

let AddButton = (parent, child_element, id_name, class_name, type, child_text) =>  {
    let child = document.createElement(child_element);
    child.id=id_name;
    child.className=class_name;
    child.type=type;
    let textnode = document.createTextNode(child_text);
    child.appendChild(textnode);
    parent.appendChild(child);
};

let SetCategory = (CateElem, CateOrder) => {
    if (CateElem.className === 'cate_btn cate_btn--off') {
	Flags[CateOrder].flag = true;
	CateElem.className = 'cate_btn cate_btn--on';
    } else {
	Flags[CateOrder].flag = false;
	CateElem.className = 'cate_btn cate_btn--off';
    }
    chrome.storage.local.set({'flag':Flags}, function() {console.log(Flags);});
};

window.onload = function () {
    let i;
    let LvCont;
    let Level = new LevelPtr;
    console.log(Level.State);
    chrome.storage.local.get(['level'], function(result) {
	Level.LoadState = result.level;
	if (Level.State === void 0)
	    Level.LoadState = 0;
	chrome.storage.local.get(['flag'], function(result) {
	    Flags = result.flag;
	    if (Flags === void 0) {
		Flags = [
		    {'name': 'PG', 'flag': false},
		    {'name': 'GAME', 'flag': false},
		    {'name': 'HIST', 'flag': false},
		    {'name': 'ENT', 'flag': false},
		];
		console.log(Flags);
		chrome.storage.local.set({'flag':Flags}, function() {});
	    }

	    let ReDrawBtn = () => {
		for (i = 0; i < 3; ++i)
		    InitChild(LvCont[i]);
		DrawButton();
	    }

	    let MoveLeft = () => {
		Level.Minus();
		ReDrawBtn();
	    };

	    let MoveRight = () => {
		Level.Plus();
		ReDrawBtn();
	    };

	    let DrawButton = () => {
		// 左のボタンを追加
		if (Level.State > LevelMIN) {
		    AddButton(LvCont[0], 'Button', 'LevelBtn-l','level_btn-s', 'button', Level.State - 1);
		    document.getElementById('LevelBtn-l').addEventListener('click', MoveLeft);
		} else {
		    AddButton(LvCont[0], 'div', 'LevelBtn-l','level_btn--dummy', '', '');
		}

		// 真ん中のボタンを追加
		if (Level.State === 4)
		    AddButton(LvCont[1], 'div', 'LevelBtn-c','level_btn-c btn-c--gold', '', Level.State);
		else
		    AddButton(LvCont[1], 'div', 'LevelBtn-c','level_btn-c', '', Level.State);

		// 右のボタンを追加
		if (Level.State < LevelMAX) {
		    if (Level.State + 1 < 4)
			AddButton(LvCont[2], 'Button', 'LevelBtn-r','level_btn-s', 'button', Level.State + 1);
		    else
			AddButton(LvCont[2], 'Button', 'LevelBtn-r','level_btn-s btn-s--gold', 'button', Level.State + 1);
		    document.getElementById('LevelBtn-r').addEventListener('click', MoveRight);
		} else {
		    AddButton(LvCont[2], 'div', 'LevelBtn-r','level_btn--dummy', '', '');
		}

		let LvDesc = document.getElementById('LevelDesc');
		InitChild(LvDesc);
		let Description = document.createTextNode(Str[Level.State]);
		if (Level.State === 4)
		    LvDesc.style.color = 'var(--red)';
		else
		    LvDesc.style.color = 'var(--black)';
		LvDesc.appendChild(Description);
		chrome.storage.local.set({'level':Level.State}, function() {});
		};

	    LvCont = document.getElementsByClassName('level_btn_box');
	    DrawButton();

	    let Category = document.getElementById('Category');
	    AddButton(Category, 'Button', 'Programming', 'cate_btn cate_btn--off', 'button', 'プログラミング');
	    AddButton(Category, 'Button', 'Game', 'cate_btn cate_btn--off', 'button', 'ゲーム');
	    AddButton(Category, 'Button', 'History', 'cate_btn cate_btn--off', 'button', '歴史');
	    AddButton(Category, 'Button', 'Entertainment', 'cate_btn cate_btn--off', 'button', '芸能');

	    let Categories = [];
	    Categories.push(document.getElementById('Programming'));
	    Categories.push(document.getElementById('Game'));
	    Categories.push(document.getElementById('History'));
	    Categories.push(document.getElementById('Entertainment'));
	    console.log(Categories);

	    for (i = 0; i < Categories.length; ++i) {
		if (Flags[i].flag)
		    Categories[i].className = 'cate_btn cate_btn--on';
		Categories[i].addEventListener('click', function(){SetCategory(this, Categories.indexOf(this))});
	    }
	});
    });
};
