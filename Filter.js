'use strict'
window.onload = function() {
    let CurrentLevel = 0;
    let i, j, k;
    let SearchLists, TitleLists;
    let DomainBlock, CategoryFlag;
    let CurrentURL;
    let NGDomainList = [], NGWordList = [], CategoryList = [];

    chrome.storage.local.get(['level'], function(result) {
	CurrentLevel = result.level;
	if (CurrentLevel === void 0)
	    CurrentLevel = 0;

	chrome.storage.local.get(['flag'], function(result) {
	    if (result.flag === void 0)
		CategoryFlag = [{flag: false, name: ""}];
	    else
		CategoryFlag = result.flag;
	    for (i = CategoryFlag.length - 1; i >= 0; --i) {
		if (!CategoryFlag[i].flag)
		    continue;
		CategoryList.push(CategoryFlag[i].name);
	    }

	    let MakeNGList = (OldList) => {
		let Buf = [];
		for (i = 0; i < OldList.length; ++i) {
		    if (OldList[i].level > CurrentLevel) continue;
		    if (OldList[i].level > 3) {
			Array.prototype.push.apply(Buf, OldList[i].list);
			continue;
		    }
		    if (OldList[i].level > 2)
			for (j = CategoryList.length - 1; j >= 0; --j) {
			    if (OldList[i].category.indexOf(CategoryList[j]) !== -1)
				Array.prototype.push.apply(Buf, OldList[i].list);
			}
		    else
			Array.prototype.push.apply(Buf, OldList[i].list);
		}
		return Array.from(new Set(Buf));
	    };

	    NGDomainList = MakeNGList(NGDomain);
	    NGWordList = MakeNGList(NGWord);

	    console.log(NGDomainList);
	    console.log(NGWordList);

	    let RemoveLink = (Links) => {
		Links.style.color = 'red';
		Links.removeAttribute('href');
	    };

	    let RemoveList = (i, Links, Title, Parents) => {
		let RmNode;

		for (j = 0; j < NGDomainList.length; ++j) {
		    if (NGDomainList[j].test(Links.href)) {
			RemoveLink(Links);
			RmNode = SearchLists[i];
			for (k = 0; k < Parents; ++k)
			    RmNode = RmNode.parentNode;
			RmNode.parentNode.removeChild(RmNode);
			DomainBlock = true;
			return;
		    }
		}
		if (DomainBlock) return;
		for (j = 0; j < NGWordList.length; ++j) {
		    if (NGWordList[j].test(Title)) {
			RemoveLink(Links);
			RmNode = SearchLists[i];
			RmNode.parentNode.removeChild(RmNode);
			return;
		    }
		}
	    }

	    let Google = () => {
		let Links, Title, Parents = 1;

		// 広告の削除
		let Ad = document.getElementById('taw');
		Ad.parentNode.removeChild(Ad);

		// ニュースや動画など削除
		let Gsection = document.getElementsByTagName('g-section-with-header');
		for (i = Gsection.length - 1; i >= 0; --i)
		    Gsection[i].parentNode.removeChild(Gsection[i]);

		// 不要な要素の削除
		let Nrgt = document.getElementsByClassName('nrgt');
		for (i = Nrgt.length - 1; i >= 0; --i)
		    Nrgt[i].parentNode.removeChild(Nrgt[i]);

		// 位置情報の入ったフッターを削除
		let Geo = document.getElementById('fbar');
		Geo.parentNode.removeChild(Geo);

		SearchLists = document.getElementsByClassName('rc');
		let Old = SearchLists[0].childNodes[0].childNodes[0].tagName === 'A';
		for (i = SearchLists.length - 1; i >= 0; --i) {
		    DomainBlock = false;
		    if (Old) {
			Links = SearchLists[i].childNodes[0].getElementsByTagName('a')[0];
			Title = Links.innerText;
		    } else {
			Links = SearchLists[i].childNodes[0].childNodes[1];
			Title = Links.childNodes[2].innerText;
		    }
		    RemoveList(i, Links, Title, Parents);
		}
	    };

	    let Yahoo = () => {
		let Links, Title, Parents = 0;

		// 不要な要素の削除
		let Cmm = document.getElementsByClassName('cmm');
		for (i = Cmm.length - 1; i >= 0; --i)
		    Cmm[i].parentNode.removeChild(Cmm[i]);

		document.getElementById('chie').parentNode.removeChild(document.getElementById('chie'));

		// 広告の削除
		let Ad = document.getElementsByClassName('ss');
		for (i = Ad.length - 1; i >= 0; --i)
		    Ad[i].parentNode.removeChild(Ad[i]);

		SearchLists = document.getElementsByClassName('w');
		for (i = SearchLists.length - 1; i >= 0; --i) {
		    DomainBlock = false;
		    Links = SearchLists[i].childNodes[0].childNodes[0].getElementsByTagName('a')[0];
		    Title = Links.innerText;
		    RemoveList(i, Links, Title, Parents);
		}
	    };

	    let Bing = () => {
		let Links, Title, Parents = 0;
		// 不要な要素の削除
		let Ans = document.getElementsByClassName('b_ans');
		for (i = Ans.length - 1; i >= 0; --i)
		    Ans[i].parentNode.removeChild(Ans[i]);

		// 広告の削除
		let Ad = document.getElementsByClassName('b_ad');
		for (i = Ad.length - 1; i >= 0; --i)
		    Ad[i].parentNode.removeChild(Ad[i]);

		SearchLists = document.getElementsByClassName('b_algo');
		for (i = SearchLists.length - 1; i >= 0; --i) {
		    DomainBlock = false;
		    if (SearchLists[i].childNodes[0].childNodes[0].tagName === 'A')
			Links = SearchLists[i].childNodes[0].getElementsByTagName('a')[0];
		    else
			Links = SearchLists[i].childNodes[0].childNodes[0].getElementsByTagName('a')[0];
		    Title = Links.innerText;
		    RemoveList(i, Links, Title, Parents);
		}
	    };

	    let Nsearch = () => {
		let Links, Title, Parents = 0;

		// 広告の削除
		let Ads = document.getElementsByClassName('gsc-adBlock')[0];
		if (Ads !== void 0)
		    Ads.parentNode.removeChild(Ads);
		
		SearchLists = document.getElementsByClassName('gsc-webResult gsc-result');
		for (i = SearchLists.length - 1; i >= 0; --i) {
		    DomainBlock = false;
		    Links = SearchLists[i].childNodes[0].childNodes[0].childNodes[0].getElementsByTagName('a')[0];
		    Title = Links.innerText;
		    RemoveList(i, Links, Title, Parents);
		}
	    };

	    let NsearchTop = () => {
		const target = document.getElementsByClassName('gsc-results-wrapper-nooverlay')[0];
		const observer = new MutationObserver((mutations) => {
		    NsearchChpage();
		});
		const config = {
		    attributes: true
		};
		observer.observe(target, config);
	    };

	    let NsearchChpage = () => {
		Nsearch();
		const target = document.getElementsByClassName('gsc-resultsbox-visible')[0];
		const observer = new MutationObserver((mutations) => {
		    Nsearch();
		});
		const config = {
		    attributes: true
		};
		observer.observe(target, config);
	    };

	    CurrentURL = location.href;
	    if (/https?:\/\/www\.google\.(com|co\.jp)\/search\?*/.test(CurrentURL)) {
		Google();
	    } else if (/https?:\/\/www\.bing\.com\/search\?*/.test(CurrentURL)) {
		Bing();
	    } else if (/https?:\/\/search\.yahoo\.co\.jp\/search\?*/.test(CurrentURL)) {
		Yahoo();
	    } else if (/pasokatu\.com\/nsearch#gsc\.tab=0*/.test(CurrentURL)) {
		/pasokatu\.com\/nsearch#gsc\.tab=0$/.test(CurrentURL) ? NsearchTop(): NsearchChpage();
	    }
	});
    });
};
