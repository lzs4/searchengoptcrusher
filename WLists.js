'use strict';

const StrictMatome_w = {
    'name': 'Matome(Strict)',
    'description': 'アフィサイト(強力)',
    'level': 4,
    'category': 'ALL',
    'list': [
	/速報/,
	/悲報/,
	/朗報/,
	/まとめ/,
	/あんてな/,
    ]
}

const Matome_w = {
    'name': 'Matome',
    'description': 'アフィサイト',
    'level': 2,
    'category': 'ALL',
    'list': [
	/【*】/,
	/wwww/,
    ]
};

const Media_w = {
    'name': 'Media',
    'description': 'いかがでしたかブログ',
    'level': 2,
    'category': 'ALL',
    'list': [
        /と話題/,
        /の理由/,
        /は[\?？]/,
        /調べてみ/,
        /調べました/
    ]
};

const PGschool_w = {
    'name': 'PGschool',
    'description': 'プログラミングスクール',
    'level': 3,
    'category': 'PG',
    'list': [
	/侍エンジニア塾/,
	/クリエイターズハイブ/,
	/プログラミングスクール/
    ]
};

const History_w = {
    'name': 'History',
    'description': 'ゲームやアニメなど',
    'level': 3,
    'category': 'HIST',
    'list': [
	/fgo/i,
	/fate/i,
	/type-moon/i,
	/モンスト/,
	/モンスターストライク/,
	/パズドラ/,
    ]
};


const Skeleton_w = {
    'name': '',
    'description': '',
    'level': 3,
    'category': 'ALL',
    'list': [
	
    ]
};

const NGWord = [
    StrictMatome_w,
    Matome_w,
    Media_w,
    PGschool_w,
    History_w
];

console.log(NGWord);
